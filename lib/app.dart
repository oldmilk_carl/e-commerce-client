import 'package:ecommerce_client/modules/product/widgets/product_screen.dart';
import 'package:flutter/material.dart';

import 'package:flutter_bloc/flutter_bloc.dart';

import 'package:ecommerce_client/modules/product/index.dart';

class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
      providers: [
        BlocProvider<ProductBloc>(
          create: (BuildContext context) =>
              new ProductBloc()..add(new ProductRefreshRequired()),
        ),
      ],
      child: new MaterialApp(
        debugShowCheckedModeBanner: true,
        title: 'E-Commerce',
        home: ProductScreen(),
      ),
    );
  }
}
