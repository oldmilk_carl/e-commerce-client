import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../index.dart';

class ProductScreen extends StatelessWidget {
  final GlobalKey<RefreshIndicatorState> _refreshIndicatorKey =
      new GlobalKey<RefreshIndicatorState>();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('E-Commerce'),
        centerTitle: true,
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          Navigator.of(context).push(
            MaterialPageRoute(
              builder: (BuildContext context) {
                return ProductAddScreen();
              },
            ),
          );
        },
        tooltip: 'Add Product',
        child: Icon(Icons.add),
      ),
      body: BlocListener<ProductBloc, ProductState>(
        listener: (BuildContext context, ProductState state) {},
        child: BlocBuilder<ProductBloc, ProductState>(
          builder: (BuildContext context, ProductState state) {
            if (state is ProductInitial) {
              return Text('Splash screen');
            }

            if (state is ProductLoaded) {
              if (state.isLoading && state.products.length == 0) {
                return Align(
                  alignment: Alignment.center,
                  child: CircularProgressIndicator(),
                );
              } else if (state.products.length > 0) {
                return RefreshIndicator(
                  key: _refreshIndicatorKey,
                  onRefresh: () {
                    BlocProvider.of<ProductBloc>(context)
                        .add(new ProductRefreshRequired());
                    return Future.value();
                  },
                  child: ListView.builder(
                    itemCount: state.products.length,
                    itemBuilder: (BuildContext context, int index) {
                      return ProductTile(state.products[index]);
                    },
                  ),
                );
              } else {
                return Text('No Data');
              }
            }

            // print(state);

            // if (state is AuthInitial) {
            //   return SplashPage();
            // }

            // if (state is AuthSuccess) {
            //   return HomeScreen();
            // }

            // if (state is AuthFailure) {
            //   // if (state.type == AuthFailureType.USER_INTENT || state.type == AuthFailureType.TOKEN_EXPIRED) {
            //   //   return new AuthScreen();
            //   // }
            //   return new ThemeWidgetsPreview();
            // }

            return Container();
          },
        ),
      ),
    );
  }
}
