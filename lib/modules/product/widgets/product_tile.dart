import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

import '../index.dart';

class ProductTile extends StatelessWidget {
  final ProductModel model;

  ProductTile(this.model);

  @override
  Widget build(BuildContext context) {
    return Card(
      elevation: 4.0,
      child: Padding(
        padding: EdgeInsets.all(20),
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    // color: Colors.amber,
                    child: Text(
                      model.name,
                      style: TextStyle(fontSize: 18),
                    ),
                  ),
                ),
                IconButton(
                  iconSize: 20,
                  icon: Icon(Icons.edit),
                  onPressed: () {
                    Navigator.of(context).push(
                      MaterialPageRoute(
                        builder: (BuildContext context) {
                          return ProductEditScreen(model: this.model);
                        },
                      ),
                    );
                  },
                ),
                IconButton(
                  iconSize: 20,
                  icon: Icon(Icons.delete_forever),
                  onPressed: () {
                    BlocProvider.of<ProductBloc>(context)
                      ..add(new ProductRemovingRequired(pid: model.pid));
                  },
                ),
              ],
            ),
            Row(
              children: <Widget>[
                Expanded(
                  flex: 1,
                  child: Container(
                    // color: Colors.red,
                    child: Text(
                      model.description,
                      style: TextStyle(fontSize: 12),
                    ),
                  ),
                ),
              ],
            ),
            SizedBox(
              height: 10,
            ),
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: <Widget>[
                Text(
                  '\$ ${model.price}',
                  style: TextStyle(fontSize: 12),
                ),
                Text(
                  'Stock: ${model.maxStock}',
                  style: TextStyle(fontSize: 12),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
