class ProductFieldValidator {
  static String validateName(String value) {
    if (value.isEmpty) return 'Name is required';

    if (value.length < 4) return 'Name must have length at least 5';

    return null;
  }

  static String validateDescription(String value) {
    if (value.isEmpty) return 'Description is required';

    if (value.length < 7) return 'Description must be more than 8 character';

    return null;
  }

  static String validateCategory(String value) {
    if (value.isEmpty) return 'Category Id is required';

    if (int.tryParse(value) == null) return 'Category Id must be an integer.';

    return null;
  }

  static String validateStock(String value) {
    if (value.isEmpty) return 'Stock is required';

    if (int.tryParse(value) == null) return 'Stock must be an integer.';

    return null;
  }

  static String validatePrice(String value) {
    if (value.isEmpty) return 'Price is required';

    if (double.tryParse(value) == null) return 'Price must be a valid number.';

    return null;
  }
}
