import 'package:ecommerce_client/modules/product/index.dart';
import 'package:ecommerce_client/modules/product/widgets/ProductFieldValidator.dart';
import 'package:ecommerce_client/shared/widgets/loading_container.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class ProductAddForm extends StatefulWidget {
  @override
  State<ProductAddForm> createState() => _ProductAddFormState();
}

class _ProductAddFormState extends State<ProductAddForm> {
  final _formKey = GlobalKey<FormState>();

  final _nameController = TextEditingController(text: '');
  final _descriptionController = TextEditingController(text: '');
  // final _categoryIdController = TextEditingController(text: '');
  final _stockController = TextEditingController(text: '');
  final _priceController = TextEditingController(text: '');

  @override
  Widget build(BuildContext context) {
    _onButtonPressed() {
      if (_formKey.currentState.validate()) {
        BlocProvider.of<ProductBloc>(context).add(ProductAddingRequired(
          name: _nameController.text,
          description: _descriptionController.text,
          categoryId: 1,
          maxStock: int.parse(_stockController.text),
          price: _priceController.text,
          ownerId: 'user',
        ));
      }
    }

    return BlocListener<ProductBloc, ProductState>(
      listener: (context, state) {
        if (state is ProductLoaded) {
          if (state.isLoading == false) {
            Navigator.of(context).pop();
          }
        }
      },
      child: BlocBuilder<ProductBloc, ProductState>(
        builder: (
          BuildContext context,
          ProductState state,
        ) {
          print(state);
          return Form(
            key: _formKey,
            child: Column(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.start,
              verticalDirection: VerticalDirection.down,
              children: [
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      labelText: 'Name'.toUpperCase(),
                      // hintText: 'Email'.toUpperCase(),
                      // hintStyle: new TextStyle(color: Colors.white),
                      // labelStyle: new TextStyle(color: Colors.white),
                      prefixIcon: Icon(
                        Icons.turned_in,
                        // color: Colors.white,
                      ),
                    ),
                    controller: _nameController,
                    validator: ProductFieldValidator.validateName,
                    keyboardType: TextInputType.text,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      fillColor: Colors.amber,
                      labelText: 'Description'.toUpperCase(),
                      // hintText: 'Password'.toUpperCase(),
                      // hintStyle: new TextStyle(color: Colors.white),
                      // labelStyle: new TextStyle(color: Colors.white),
                      prefixIcon: Icon(
                        Icons.description,
                        // color: Colors.white,
                      ),
                    ),
                    controller: _descriptionController,
                    validator: ProductFieldValidator.validateDescription,
                    keyboardType: TextInputType.text,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      fillColor: Colors.amber,
                      labelText: 'Stock'.toUpperCase(),
                      // hintText: 'Password'.toUpperCase(),
                      // hintStyle: new TextStyle(color: Colors.white),
                      // labelStyle: new TextStyle(color: Colors.white),
                      prefixIcon: Icon(
                        Icons.business,
                        // color: Colors.white,
                      ),
                    ),
                    controller: _stockController,
                    validator: ProductFieldValidator.validateStock,
                    keyboardType: TextInputType.number,
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30, 0, 30, 0),
                  child: TextFormField(
                    decoration: InputDecoration(
                      fillColor: Colors.amber,
                      labelText: 'Price'.toUpperCase(),
                      // hintText: 'Password'.toUpperCase(),
                      // hintStyle: new TextStyle(color: Colors.white),
                      // labelStyle: new TextStyle(color: Colors.white),
                      prefixIcon: Icon(
                        Icons.attach_money,
                        // color: Colors.white,
                      ),
                    ),
                    controller: _priceController,
                    validator: ProductFieldValidator.validatePrice,
                    keyboardType: TextInputType.number,
                  ),
                ),
                Padding(
                  padding: EdgeInsets.all(30),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      LoadingContainer(
                        isLoading: (state as ProductLoaded).isLoading,
                        loadingWidget: CircularProgressIndicator(),
                        child: Expanded(
                          child: RaisedButton(
                            child: Text('Add Product'),
                            onPressed: !(state as ProductLoaded).isLoading
                                ? _onButtonPressed
                                : null,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          );
        },
      ),
    );
  }
}
