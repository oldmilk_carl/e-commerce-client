import 'package:flutter/material.dart';

import '../index.dart';

class ProductEditScreen extends StatelessWidget {
  final ProductModel model;

  ProductEditScreen({this.model});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Edit Product'),
        centerTitle: true,
      ),
      body: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          verticalDirection: VerticalDirection.down,
          children: <Widget>[
            ProductEditForm(model: model),
          ],
        ),
      ),
    );
  }
}
