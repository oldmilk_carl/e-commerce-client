import 'dart:convert';

import 'package:equatable/equatable.dart';

/// use https://marketplace.visualstudio.com/items?itemName=BendixMa.dart-data-class-generator
class ProductModel extends Equatable {
  final String pid;
  final String name;
  final String description;
  final int categoryId;
  final int maxStock;
  final int currentStock;
  final String price;
  final String ownerId;

  ProductModel({
    this.pid,
    this.name,
    this.description,
    this.categoryId,
    this.maxStock,
    this.currentStock,
    this.price,
    this.ownerId,
  });

  @override
  List<Object> get props => [
        pid,
        name,
        description,
        categoryId,
        maxStock,
        currentStock,
        price,
        ownerId,
      ];

  factory ProductModel.fromJson(Map<String, dynamic> json) {
    return ProductModel(
      pid: json['pid'],
      name: json['name'],
      description: json['description'],
      categoryId: json['categoryId'],
      maxStock: json['maxStock'],
      currentStock: json['currentStock'],
      price: json['price'],
      ownerId: json['ownerId'],
    );
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['pid'] = this.pid;
    data['name'] = this.name;
    data['description'] = this.description;
    data['categoryId'] = this.categoryId;
    data['maxStock'] = this.maxStock;
    data['currentStock'] = this.currentStock;
    data['price'] = this.price;
    data['ownerId'] = this.ownerId;
    return data;
  }

  @override
  String toString() {
    return json.encode(toJson());
  }
}
