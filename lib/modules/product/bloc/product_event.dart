import 'package:ecommerce_client/modules/product/index.dart';
import 'package:equatable/equatable.dart';
import 'package:meta/meta.dart';

@immutable
abstract class ProductEvent extends Equatable {
  @override
  bool get stringify => true;
}

class ProductRefreshRequired extends ProductEvent {
  @override
  List<Object> get props => null;
}

class ProductRefreshed extends ProductEvent {
  final List<ProductModel> products;

  ProductRefreshed({this.products});

  @override
  List<Object> get props => products;
}

class ProductAddingRequired extends ProductEvent {
  final String name;
  final String description;
  final int categoryId;
  final int maxStock;
  final String price;
  final String ownerId;

  ProductAddingRequired({
    this.name,
    this.description,
    this.categoryId,
    this.maxStock,
    this.price,
    this.ownerId,
  });

  @override
  List<Object> get props => [
        name,
        description,
        categoryId,
        maxStock,
        price,
        ownerId,
      ];
}

class ProductAdded extends ProductEvent {
  final ProductModel product;

  ProductAdded({this.product});

  @override
  List<Object> get props => [product];
}

class ProductRemovingRequired extends ProductEvent {
  final String pid;

  ProductRemovingRequired({
    this.pid,
  });

  @override
  List<Object> get props => [
        pid,
      ];
}

class ProductRemoved extends ProductEvent {
  final String pid;

  ProductRemoved({this.pid});

  @override
  List<Object> get props => [pid];
}

class ProductUpdatingRequired extends ProductEvent {
  final String pid;
  final String name;
  final String description;
  final int maxStock;
  final String price;

  ProductUpdatingRequired({
    this.pid,
    this.name,
    this.description,
    this.maxStock,
    this.price,
  });

  @override
  List<Object> get props => [
        pid,
        name,
        description,
        maxStock,
        price,
      ];
}

class ProductUpdated extends ProductEvent {
  final ProductModel product;

  ProductUpdated({this.product});

  @override
  List<Object> get props => [product];
}
