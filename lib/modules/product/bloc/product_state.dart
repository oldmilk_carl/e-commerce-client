import 'package:equatable/equatable.dart';

import '../index.dart';

abstract class ProductState extends Equatable {
  final List propss;
  ProductState([this.propss]);

  @override
  List<Object> get props => (propss ?? []);

  @override
  bool get stringify => true;
}

/// Initialized
class ProductInitial extends ProductState {
  final List<ProductModel> products = new List<ProductModel>();

  @override
  List<Object> get props => (products);

  @override
  String toString() => 'ProductInitial $products';
}

// class ProductInProgress extends ProductState {
//   @override
//   List<Object> get props => [];

//   @override
//   String toString() => 'ProductInProgress';
// }

class ProductLoaded extends ProductState {
  final bool isLoading;
  final List<ProductModel> products;

  ProductLoaded({this.products, this.isLoading});
  @override
  List<Object> get props => [isLoading, products];

  @override
  String toString() => 'ProductLoaded $products';
}

class ErrorProductState extends ProductState {
  final String errorMessage;

  ErrorProductState(this.errorMessage) : super([errorMessage]);

  @override
  String toString() => 'ErrorProductState';
}
