import 'dart:async';
import 'dart:developer' as developer;

import 'package:bloc/bloc.dart';
import 'package:flutter/foundation.dart';
import 'package:signalr_client/signalr_client.dart';

import 'package:ecommerce_client/constants.dart';
import 'package:ecommerce_client/modules/product/index.dart';

class ProductBloc extends Bloc<ProductEvent, ProductState> {
  String _productUrl;
  HubConnection _hubConnection;
  bool _productConnectionIsOpen;

  ProductBloc() : super(new ProductInitial()) {
    _productUrl = MyConstants.serverURL + "/producthub";
    _productConnectionIsOpen = false;
    // openProductConnection();
  }

  @override
  Future<void> close() async {
    // dispose objects
    await super.close();
  }

  Future<void> openProductConnection() async {
    if (_hubConnection == null) {
      _hubConnection = HubConnectionBuilder().withUrl(_productUrl).build();
      _hubConnection.onclose((error) => _productConnectionIsOpen = false);
      _hubConnection.on("OnProductRefreshed", _handleOnRefreshed);
      _hubConnection.on("OnProductAdded", _handleOnAddedProduct);
      _hubConnection.on("OnProductRemoved", _handleOnRemovedProduct);
      _hubConnection.on("OnProductUpdated", _handleOnUpdatedProduct);
    }

    if (_hubConnection.state != HubConnectionState.Connected) {
      await _hubConnection
          .start()
          .then((value) => _productConnectionIsOpen = true);
    }
  }

  Future<void> refreshProducts() async {
    await openProductConnection();
    _hubConnection.invoke("Refresh");
  }

  void _handleOnRefreshed(List<Object> args) {
    List<ProductModel> data = new List<ProductModel>();
    if (args[0] != null) {
      args.forEach((v) {
        data.add(new ProductModel.fromJson(v));
      });
    }

    add(new ProductRefreshed(products: data));
  }

  Future<void> addProduct({
    @required String name,
    @required String description,
    @required int categoryId,
    @required int maxStock,
    @required String price,
    @required String ownerId,
  }) async {
    await openProductConnection();
    print(price);
    _hubConnection.invoke("Add", args: <Object>[
      name,
      description,
      categoryId,
      maxStock,
      price,
      ownerId,
    ]);
  }

  void _handleOnAddedProduct(List<Object> args) {
    ProductModel newProduct = new ProductModel.fromJson(args[0]);
    add(new ProductAdded(product: newProduct));
  }

  Future<void> removeProduct({@required String pid}) async {
    await openProductConnection();
    _hubConnection.invoke("Remove", args: <Object>[
      pid,
    ]);
  }

  void _handleOnRemovedProduct(List<Object> args) {
    String pid = args[0];
    print('Remove pid: $pid');

    add(new ProductRemoved(pid: pid));
  }

  Future<void> updateProduct({
    @required String pid,
    @required String name,
    @required String description,
    @required int maxStock,
    @required String price,
  }) async {
    await openProductConnection();
    _hubConnection.invoke("Update", args: <Object>[
      pid,
      name,
      description,
      maxStock,
      price,
    ]);
  }

  void _handleOnUpdatedProduct(List<Object> args) {
    add(new ProductUpdated(product: new ProductModel.fromJson(args[0])));
  }

  @override
  Stream<ProductState> mapEventToState(
    ProductEvent event,
  ) async* {
    try {
      if (event is ProductRefreshRequired) {
        yield new ProductLoaded(
          isLoading: true,
          products: state is ProductLoaded
              ? (state as ProductLoaded).products
              : new List<ProductModel>(),
        );

        await refreshProducts();
      }

      if (event is ProductRefreshed) {
        yield new ProductLoaded(
          isLoading: false,
          products: event.products,
        );
      }

      if (event is ProductAddingRequired) {
        yield new ProductLoaded(
          isLoading: true,
          products: state is ProductLoaded
              ? (state as ProductLoaded).products
              : new List<ProductModel>(),
        );

        await addProduct(
          name: event.name,
          description: event.description,
          categoryId: event.categoryId,
          maxStock: event.maxStock,
          price: event.price,
          ownerId: event.ownerId,
        );
      }

      if (event is ProductAdded) {
        List<ProductModel> newList = (state as ProductLoaded).products;
        newList.add(event.product);
        yield new ProductLoaded(
          isLoading: false,
          products: newList,
        );
      }

      if (event is ProductRemovingRequired) {
        yield new ProductLoaded(
          isLoading: true,
          products: state is ProductLoaded
              ? (state as ProductLoaded).products
              : new List<ProductModel>(),
        );

        await removeProduct(
          pid: event.pid,
        );
      }

      if (event is ProductRemoved) {
        List<ProductModel> newList = (state as ProductLoaded).products;
        newList.removeWhere((item) => item.pid == event.pid);
        yield new ProductLoaded(
          isLoading: false,
          products: newList,
        );
      }

      if (event is ProductUpdatingRequired) {
        yield new ProductLoaded(
          isLoading: true,
          products: state is ProductLoaded
              ? (state as ProductLoaded).products
              : new List<ProductModel>(),
        );

        await updateProduct(
          pid: event.pid,
          name: event.name,
          description: event.description,
          maxStock: event.maxStock,
          price: event.price,
        );
      }

      if (event is ProductUpdated) {
        List<ProductModel> newList = new List()
          ..addAll((state as ProductLoaded).products);

        ProductModel productModel =
            newList.singleWhere((item) => item.pid == event.product.pid);

        ProductModel newProduct = ProductModel(
          pid: productModel.pid,
          name: event.product.name,
          description: event.product.description,
          categoryId: productModel.categoryId,
          maxStock: event.product.maxStock,
          currentStock: productModel.currentStock,
          price: event.product.price,
          ownerId: productModel.ownerId,
        );
        int index = newList.indexWhere((item) => item.pid == event.product.pid);
        newList.replaceRange(index, index + 1, new List()..add(newProduct));

        yield new ProductLoaded(
          isLoading: false,
          products: newList,
        );
      }

      // yield await event.applyAsync(currentState: state, bloc: this);
    } catch (_, stackTrace) {
      developer.log('$_',
          name: 'ProductBloc', error: _, stackTrace: stackTrace);
      yield state;
    }
  }
}
