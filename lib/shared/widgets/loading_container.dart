import 'package:flutter/material.dart';

class LoadingContainer extends StatelessWidget {
  final bool isLoading;
  final Widget loadingWidget;
  final Widget child;
  LoadingContainer({
    @required this.isLoading,
    this.loadingWidget: const CircularProgressIndicator(),
    @required this.child,
  });

  @override
  Widget build(BuildContext context) {
    return isLoading ? loadingWidget : child;
  }
}
